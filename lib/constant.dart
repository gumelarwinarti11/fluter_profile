import 'package:flutter/material.dart';

class ColorPallete {
  static const primaryColor = Color(0xff5364e8);
  static const primaryDarkColor = Color(0xff607Cbf);
  static const underlineTextField = Color(0xff8b97ff);
  static const hintColor = Color(0xffccd1ff);
  static const whiteSmoke = Color(0xf5f5f5f5);
}
