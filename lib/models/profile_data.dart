class Profile_model {
  int _id;
  String _username;
  String _nama;
  String _deskripsi;
  String _gambarProfile;

  // konstruktor versi 1
  Profile_model(
      this._username, this._nama, this._deskripsi, this._gambarProfile);

  // konstruktor versi 2: konversi dari Map ke profile_model
  Profile_model.fromMap(Map<String, dynamic> map) {
    this._id = map['id'];
    this._username = map['username'];
    this._nama = map['nama'];
    this._deskripsi = map['deskripsi'];
    this._gambarProfile = map['gambarProfile'];
  }
  //getter dan setter (mengambil dan mengisi data kedalam object)
  // getter
  int get id => _id;
  String get username => _username;
  String get nama => _nama;
  String get deskripsi => _deskripsi;
  String get gambarProfile => _gambarProfile;

  // setter

  set username(String value) {
    _username = value;
  }

  set nama(String value) {
    _nama = value;
  }

  set deskripsi(String value) {
    _deskripsi = value;
  }

  set gambarProfile(String value) {
    _gambarProfile = value;
  }

  // konversi dari profile_model ke Map
  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = Map<String, dynamic>();
    map['id'] = this._id;
    map['username'] = username;
    map['nama'] = nama;
    map['deskripsi'] = deskripsi;
    map['gambarProfile'] = gambarProfile;

    return map;
  }
}
