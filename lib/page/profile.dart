import 'dart:async';
import 'package:flutter/material.dart';

import 'package:flutter_profile/page/edit_profile.dart';
import 'package:flutter_profile/helpers/dbHelper.dart';
import 'package:flutter_profile/models/profile_data.dart';
import 'package:sqflite/sqflite.dart';

class Profile extends StatefulWidget {
  @override
  ProfileState createState() => ProfileState();
}

class ProfileState extends State<Profile> {
  DbHelper dbHelper = DbHelper();
  int count = 0;
  Future<List<Profile_model>> contactList;

  @override
  Widget build(BuildContext context) {
    // if (contactList == null) {
    //   contactList = List<Profile_model>();
    // }

    return Scaffold(
        appBar: AppBar(
          title: Text(
            'Indashafa_',
            style: TextStyle(fontSize: 18),
          ),
          leading: IconButton(
            icon: Icon(Icons.add),
            onPressed: () {},
            color: Colors.black,
            iconSize: 30.0,
          ),
          actions: [
            Padding(
              padding: EdgeInsets.all(8.0),
              child: IconButton(
                icon: Icon(Icons.menu),
                onPressed: () {},
                color: Colors.black,
                iconSize: 30.0,
              ),
            )
          ],
        ),
        body: Column(
          children: [
            Container(
              padding: EdgeInsets.all(15),
              margin: EdgeInsets.only(bottom: 20),
              decoration: BoxDecoration(
                border: Border.all(
                  color: Colors.grey,
                  width: 0.1,
                ),
              ),
              child: Center(
                child: Text(
                  'See Covid-19 Business Resources',
                  style: TextStyle(color: Colors.cyan, fontSize: 12),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 10),
            ),
            Row(
              children: [
                // https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png
                kotakPhoto(
                    'https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png'),

                kotak('0', 'Posts', 16, 12),
                kotak('124', 'Followers', 16, 12),
                kotak('166', 'Following', 16, 12)
              ],
            ),
            Container(
              alignment: Alignment(-0.98, 0.0),
              padding: EdgeInsets.symmetric(horizontal: 16),
              margin: EdgeInsets.symmetric(vertical: 20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  new Text(
                    'Your Name',
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
                  ),
                  Padding(
                    padding: EdgeInsets.all(4),
                  ),
                  new Text(
                    'Your Description',
                    style: TextStyle(fontSize: 13),
                    textAlign: TextAlign.justify,
                  )
                ],
              ),
            ),
            Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  OutlineButton(
                    padding: EdgeInsets.symmetric(horizontal: 26),
                    onPressed: () async {
                      var profile = await navigateToEntryForm(context, null);
                      if (profile != null) addContact(profile);
                    },
                    child: Text(
                      'Edit Profile',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                  ),
                  button('Promotions'),
                  button('Insights')
                ],
              ),
            ),
          ],
        ));
  }

  // kumpulan fungsi fungsi
  // data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAoHCBYWEhgWFRUYGBgVHBoYGBIYEhIYEhgYGBgZGhgYGBgcIS4lHB4rIRgYJjgmKy8xNTU1GiQ7QDs0Py40NTEBDAwMEA8QHBISGjEhISE0NDExMTQ0NDE0NDQ0MTE0NDQ0NDQ0PzQ0NDQ/NDE/NDQxNDExMTQ0MTE0MTQxMTExMf/AABEIAOEA4QMBIgACEQEDEQH/xAAbAAACAgMBAAAAAAAAAAAAAAAEBQMGAAIHAf/EADsQAAECBAMGAwYGAQMFAAAAAAEAAgMEESEFEjEGIkFRYXEygZETI1KhscEzQnLR4fBiFDSCByRTkqL/xAAZAQADAQEBAAAAAAAAAAAAAAABAgMABAX/xAAhEQADAQACAwADAQEAAAAAAAAAAQIRITEDEkEiMlEEE//aAAwDAQACEQMRAD8ADirwGy8mnUQbo6TDr9sNMQcKKo4jqrHORrKtzxutgrrSCAnci5JICbyZSsdFklnpHjDgHVKbyhsq9tNFocvNOhbeIVxpmt9OiHiRKqCqwOTI5gvPRtOd/TRQuNlq130WzBVbQGzH3qjWioPCv2CFbANVOxjhY9fVD2H9eDeG3dvwN/3RjYNR/dUNLitQeI80fIuN2nUXHWiZCA8KjXUOh48A7knUiygLTdr+HJw0S6ZgXI4EVp1THCt4EE3Ar5hMY3l92IOuh+x9FYWRAx2cDcdQPFK04ZwkzmAg11FweiYYbMbtHaU0HWxWw2lokXhht/Br+6lxeUBAit42cOvNJpCLlZl19mbHkw6eiskk/M0sdcP9KpKQ8V6sVVso4hopo8MscWnUIeKNFNnenq0IhuqAj4JS+AExhBYFFT22ZuErnrF0jbNvuz2XN4aDFRusWyxKMXCe0KUPcnE6EvbAqqHLTFky80SSZN1Z5qWoNFXJ9lCswS9B4ITaSSiFqmskfqlLSWKU0VV2mPvqdFZ5Z9G15X9FS8ViF8VziLE27JpE8vQCvaL0hSwIdU7Zzo8gwydE9k8LqBUKXDcOFakJ2Kt8LangFCqLxH9BoOEV4LyPhPRNWf6gjdYD0W7J57DSLBIrxugtHeIqkWQcx2h78OyIZDuDevJXOWhy8xZrqO+A1BQU/gjmcDQaG6pNYTqU+hNHYDQ6ZT8lDLwy12Zti00PUI4tOWh8TbHqFNh8DM8gaOB9VT2J+po14JBHE0PmvZN9HEV0sfNQTcAsdTSpBr5ocRMrw74t0j6JlWiuWh9AmQHA1pUUPcaJ3ITu41/wupToFTmzFKA3um8hHORwGmYn+EGzFxxRuZrYnMUKVPajpCYESDl6VHcIQtUmdnhrVhvCFkdAKAho2CVkUYg2y/DPZc0YunbXD3Z7LmTUtCIkqsXn90XqGhLlOFBwnoufak7XkFPpzWuQubfuqq4kbp7MPNEgnQsLPYJCTaS180phJpJm/mgdMlmkZXOw10FLcyeCTTuG5y9wuG1FeFuSsWFgmDlb4nvLRzrlspMSlGQYeQvAdxIFST2U3T0fFhzWZlSx1CmGEy9SDRE4rCBawi/XiUbgkvYdk7rZOb0/Ie4dI1CaCTDLlH4TLDKLcAicXw/NDIp5qL0uuBbKT7AfEwd4jRVWj/TQ4sPfa0tOjw4ObXuNFzWf2Pe+HSEWkg1o43PRPdjcBmWOdmd7D2baZC4eze+lrHUKkzq7EquehjM7PMDwW2oag/ymErLlzcj7jgsko+cO4FhIc34XftyRmHXKT2aYfXVogmNnyXaU61QkphDocStK0N+x4hdAjQhS4SeelS5py0BITewuCnF8AzXA10VBxeScwubShbQjt0V+ZPTMM7zTEYNRl4dClO0sNkcCJDaWvbqwgg0TS8FrGUtseoa7rlPdN5CYIoL3P1QT5WliONehuiYBuBTQ/NOmSaLbs68ht+GYeVUa5qFwMe6zdboscf7qszo8K7NK3RcFBubdFy5QLPoV7VsrDPZcvAv5rrG034R7FcnHiPcoMVEixeLEA4XabZZJnMurBMtslTmXKY5r7AosOyQz7VaIrbKuYm3VYSexVDTGUS6HqmMoLrYdMlu2aie9ht/ycR3yovEACxzjdxJArw4JXgsNznVYaOYcze44JzPy7ny7y2gia5eZUn2OVPFJQtIBuaVPLyReEi4C2xoUbDqauyDN0IWuFC6PwT15L/g/DsFa4MqHNoR8lUcCf9leJOlEqNbxCiYwahqw5T6j0WMgxhrkI6tKsTgEvnYrQDUhFrOhJt1xgniQmjOQwNe8BriBQGmhoj8Gk6CqBhuD3a6qxyEOjUJnWHyVkgs42jSeWpVJxPa2FBiBrw4B3hORxLuFgFfZuHUEUrXgqNi2zkCZib4cx7TZwcQRe1K2R9VvIst+vBY8GxSDGZWG5ryPEzR7e7Tda4thLXtzNAB5pBK7JPgNL4b80bMC2IDl3WjwEcaq44c8vh1e0tcfE08DS6Zr+C6+2cw2llmwy05dWlxHUKtyEXO/JloTW/0Vt2+eP9TkH5WV86Ku7PypEQE87earC4Ep8l1lYYZCaK+O9FlULHefaBtbNsEUDZBnX41knp4KaXUDjZTyxSjME2jb7o9iuTHxHuV17H2+6PYrkcQbx7lZio9WLaqxAY6BFFkte25TKJogIxTacvk7BY+irWJ8VZYgJCr2KQrIoSexKzVMJZL2aphLLM6JZZMDmsjw7hp6q2Ncx7atcL63VIlCoNo4Tsge1xGXUAkV9Ckc6UbxB+0LAIlA4G3DgtsMZbySCFWgudBxKtGECynSzgC5LPgr8uvRXOVjjKqZhzPknrJxrQc3Cg7nkEqZqHESfOgB7quxZ8vmsjhuj0KMhYg1wNx2Q0WWa9wcRfSo1TCysGUtOy74lGPhl7dWNe0uHkFYpd4pZc/h7MMbEbGhsDXtNyKAkcQeaezs7MwoYdBhseR4mviFtB0IF08vGJc+y4LI8XQ0eRY/UX+ailpwvhNeW5XECra1oeIqjJeICjw2Q/KUQwpAN0J7G6LayikQs/Eyw3ka0IHcigT+qQrqqOR444xZqK8aElo7Nt9lPhEtq42FqeSbvwvJlFKl9K9K3Wk6xrIhYzwtpbyFUFa3EXnwvU2LiaxPmmDdEC8UcjmGwW3Tq64NiLKWUddRubZbSouiZnmOfhnsVySMN93crruNNrDPYrk0yPeO7oMVEaxerEocL/FNAl7mFxTQwqqSDK3WOXydgUOV3dEjxeVoCruIIa1VbHjai2k57KIW7x7o6WCDeN890xlWE6BPml5GMsEVPtrDv1QbYzGDfeB0rU/JRx8YhvGRmYk8aUC2YU1ZyBnUdgrHgz9EijM0R2FRqFRoaS7SsTKe9E5mZZjodHXrfW9e6rUtErRPTFqyiRIDFrMGeDWHGff8rqOA80dLiZZqA8DiBdEyDiCrLJOY4XF+apLRvf1+AuH4i1zRmGVw1BFEzhta7Q1UrpRjhQtB8vugzhWQ1Y5zeQzEj0VGiDua64YRMUDaaJdhU3vOadWmi3jmK1oDg13UVFuoSrCYb/axHHSoAPOgUaePgeUnLTLe19UsxaM4Pht/KScw52siZd5KjmJarw91A1tSelE7dUsIKVNcirEMrHOe7RgqK8SRugdVVXOzOqdTc+aJxnFnRolrMB3RzpxKFhOWU+p2TrS0gmRQomXNgoJ06KSVO6Ew4e1eQdVjdPJeQvEjpiWfbWGex+i5LiApFeOq69Nj3Z7Lk2MNpGcs+hF2B0WLFiUbTpkNTtiAIEOoEPFmkzRyWuRhMzQoqfjczWqaxopcLJBiIaDRxqfh/dZS2JMtsUy0vmOZ2ldOf8IqYfQUbYchYLeTua+nbkgp+OGuI6ldClSjsUqZ1gcyVJgkPNEryCEc8uNhVPdnpcgV51Cl5KIb7UFRIfyUMMljqpwYCCjwbrlbOhDeQmagXVklHZgFSZSJkKuGERqoGaH8qxOpPRLZYBNYLdEUyddDKGpSUPDKmqqqjkpckEe6gawDQW7KWO5VraHaJsIFjDWIdTwYOZPNI1paE8LJIzLHOc0EEtND0PJA7XTBZLGhpmcG68DWv0XO5HEIkNxcx5BNzxB7refxiPHcBEdUNNQ0Cja8yqS8GfhftpuG3UzDRRtUqJ1GTDatBXsobL0ndWsuLoADhovZfVeNWQzdZAfQdHO4VybHW+/cusRm7hXK9oWUjlFifRasWLEox0F2iXRga2RMefaBuip5mzUqm5x5Gob+ldK8LZN+GnyD4nOPZuMs6lXO40PAKvxjSv71KYYdV7XkmpBIqh56FQJ1KSKzCUnuFvFUccEY9xe69b04JNKPo4HqrdKvqyiTyczwCuZEc1AaxtGgDlbmmWFS2VgUcaFmeB1T6Tl6NC5KZKUQ+yQsxL2TssQ8xCUypXTCTLDphzDfRSGVujpaU5hBsZMsWF4m00qeSscvHB0VOgYaK168LJzLSjho5yyoSlpaGPChmZ5jGlz3BrRqSaBVDarHBKQQcxMSJZja+rj0F1VcbiOf7OIXEtezQmwc3xWVlDc6Tnw7yP8AaHbEuqyXsP8Ay8SP8R9yqrAaSSSSa3qTeqgRMuVkWmVPAVDstIrrralVo7giEZwDUAqdyElDuoqiwTZui9hWK2haLRrqlYDDG6LwWK9YV6W3RMGl24uY7Tj3/kunQxVhXONrYdIo80X0J9Edli0qsShLS8ilkLGuNEQ48kPE+y9NnSxfgho+IzzCmn2VCEgRAyaBOjxlJ70TWahWKn8wnPKwq7xQqzYFMAih7JFNwqeSkwqYyvHdTa+Cpc4WyDK1iJ62DQAUQuDMzvBCexoFKLhtY2iLWMBbBNNEC9tHZT6p6Idkuiw966lo6IYMEFNZWUCjgS1dE1lZa2qGmZJBlwEU+jGuc4gNaCS42oBclbQ2Cipv/U7EnMlDBYaGMaOPHIDvDz0TwtZOq4Od7TY2ZubdE/INyG3gGNJoe51Voa3Nh7HfA+n/ALALn0i29Oq65sZDBlXBwBrwOnBd/jn2nC3i5kqhCngKyzOzzIlfZkw3jWG47vkUmj4ZFhHfYQODhdp81OvFUhzDISirfzUkIrSKaOUgMPlijG6IKAjWaLGR7SyghHeRLAo2tusEKh6olqHYpzoiKwuBoVz3bMe8C6FK6FUDbUe8aUX0J9KxRYsqsSjaWVwooHlEvCFjlemzqYgxQeo0VgkI/tYIfxbuu7hIZ9HbIxxnfCJ8Yq0dQpblEU/Wj2dg+iVBuUq1TkvqKf2irseFThdakNU/S1bK4jle0k2rvdlf47AQCNNQei4/hUyWuAXUtnZv2kLITVzRVp5jl5Ln80atQlrVocyHYIWNKVKcS8HcFV4+EuJoknyLZeXITKC2ywNXpQwzZtEeACuZ7dzGeIL2aKK+TzjQrne0cMkklVlciNaVGRh+8Pddb2ehZZXlX+FzHCoFYn/JdblWZZdo6L0PAvp1+OckiZFzXPjZx4kIlk6dDccjevdCsFHZvI9l7MkNdThqPNdWJ9lHJ5M4PCiHMzcfy/IfLgqvjcm+E8B7eztWnzVogxEwbkiMLIjQ5p4H7KF+CX0SqCm4e4liYQ9FtMYWYD8ouw3YenI9lq1cVS5eMmiWGtIllswXWzwl0Y2boiBoh4YRI0W0VoKlDqqRtsy4P91V1k9VUduBoUfgj7KTVYsr0WJRi2Pago6PKCmWr02dbEc+EtgR3MiNe2xaQfQ6JxNw0omId1z1u6c1cM6I8tiMD26OFadeKQ4nK1uAhNmcYDPdxPA47rvhPXorRMwQ5ppSnA61VU1SLzSpFMYaOqrZs9iJY9pBuDUfcJBOy1HWW8m/K4VtTilz4xeOmdzw+M2JDa9vEXHIr2JBqueYJtOyCQA/NXVgIPn0V1kNooESlHZSfynnyqFzX4c5RyXNS9XIYZfotvYCmiNa1eOao+n9Je4jnIJvQKnbQygYxzn+Sv8ANPDRdcz2rmy9r3CuVtWg8CR4vK4RidrCsPWJNnIGaKDzcSukxBSGB0VO2Qlt5ruSus01en4pxHdP8BIYtdCz7/dtdyOU/ZFEUCBjNzh7PiFW9wqjtEEOYFU0k4tVWZJ+ZyaQI1HEV0KGiFknYXtILhxaKjuFWIJVnw2JUUr/AEquRoeWI5vIkfNcf+iemQaxm7dVrGWmahW0Q2XKYkglEjRCQSpy6yJgmVddVnbcbn95qxSmqRbZD3ZR+En2UBYtM5WJQ6WyG6q1jMXsDVTPavS7OzsURoSXxZWqfuhVWgl9UjnRKnSrvlqG6aYeyI1u5Ec0fDqPQoyalKjT5ISUeWOCX1xiqcZkx7Y+J/o1oS8wCSWuJNdDVWr2Ye2oS+Ykr2RchciKCwtdqadDRM2zuR4LSdAQa3B6ryYl8prS1b2WsaVFARx4JMYvriO+4PMZ5eG/4mMPq0VREV1kg2Mif9hBqdGUv0KFxbaZoeYcNudws59dxp5V4qDlt4jj/wCbdNIh2rxHJDN70NFUcTglsu1hsRAY93Hee8uPyRuLPz77qEg6G7ewC0bFa+HGJN3Q6BliWtYLALojxevJ1R4vXkY7LSoENrk9mEPg0MCE2hGg0INLDVFRtF1SWT5FsbRARX5XB3JMI54JJPTLBulwqbUBBPoNFm8HbCYcoGPe4eEjMDwulrI9HOKaRYmWAGnjp2VdixOCRsxb8Cmt17ibBQ4p+MT8QDvUBCSLiJcDi94CLxj8QdGN+ij5/wBSNrkEcpGiy1AspmaLiEI4SIAUDRdTseEUK9Npd1HJVtZDrDPZM4fiQm04rDPZHBGcz9msUlCvVsFLEw7wqi3aIRrNCiuHkvQR3ojK9aF7RetRM0eOalk5L0JICbi6ijQqhBoGAOHR6WKbZQ66SPhEFHyUc2qgjI3nZSrXW6pe6HuX8k+fFBrXkkU9E/K25PyWaBRbJDG/ZyDGNdvEOaBx11QMiz1NzzSaTghjRmPDTUiv0RxxI6NZpxJP2SzMz2CIzkZOaAd8Eg3z0sL8lJh0vAMR73CzG5tbFnHRJXT0R1t0D9K1aHkUrQHUC1Ry7Jm9HYrbOxmRHPgvewOcSANKE2BaeiYnaudNi9nf2TarUSyhbL3Sr2ExkUfEZmJXPFf2GVo/+QjcDk6vqdTqf5UbodE/wKCAC7kCnSGlG2MRRZo4Cirzn1eO6OxGOSSUvk2lz+yzYxbpaHuQh1Lj9ETjg99/xb9EIYnvWMb+QNr3NyERj7vfdmt+il5/1I+T4DVstmPUAdZbsK4hCZpupW6oZhupWOW0DJB4vNR42Kwj2Ug1WuMD3R7FMJSOfezWKTKViOijNlwpctlDA1RC70dyMAWUXrVsETaaMUjVrRbtosEEjwlG0URcVD5OnO6DBhFPTORhNf6F7gkrmGd9Mzt5oOlK9UtxPfiMhjVzgPJW50Foa1tLNbl4dKpeycrWLDAq5x5kkdlqyD0TOLCFBRRZOiPqVB2QPqpzCoFM1i1eUxgKOFtJyjn+FpNLmg0UgYHRGAmjSRVx0AXRMEwNkNj8rmua8CjqDMQRep+iWq9SHl8qjs5fEZvUT2GQyBXQuoPJeT+APZEe8U9mH5WX3i3mOdNFBjETKGs+EXTS01pSKVLUJJ16nwSFWIOpCAjuqU8wIZQ9/wADSfOlAh9G0ZYRvzTnDQvPo232W2OxKzDxyoPQKTYqBUlx/tShcSdWPEP+R+Sj53+JG3+WHkPRbtWsE2Ww1XEKSM1W7VCStmuuiYmc663xG8I9lCdURNtrCPYphKKV7NYjMqxbGIaQdfIqYLFi9BHabHgtBovViJjZy9Z916sWGI4i1GoWLFjCmH/v4Xc/RWqL4lixKuxI7Z7FUYWLEw5I1DxNV6sWMRQ9R5rqeAf7SH+hv0CxYpeU4P8AX8K5tn4oH6nKn4z4j5L1Ymj9S/8Am/UROT2Q/Ai/o/ZYsRRYsmxfhKTTn4r/ANTvqvVij5+iF/seQVK1YsXGYx2q2bqsWIoxI5FRvwj2KxYmQlFYWLFiIp//2Q==

  void query() async {}

  void nama() {
    print(contactList);
  }

  Future<Profile_model> navigateToEntryForm(
      BuildContext context, Profile_model profile_model) async {
    var result = await Navigator.push(context,
        MaterialPageRoute(builder: (BuildContext context) {
      return EditForm(profile_model);
    }));
    return result;
  }

  //buat contact
  void addContact(Profile_model object) async {
    int result = await dbHelper.insert(object);
    if (result > 0) {
      print('data berhasil ditambah');
    }
  }

  Widget kotak(String teks1, String teks2, double size1, double size2) {
    return Container(
      child: Column(
        children: [
          Text(
            teks1,
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: size1),
          ),
          Text(teks2,
              style: TextStyle(fontSize: size2, fontWeight: FontWeight.bold))
        ],
      ),
      margin: EdgeInsets.all(14.5),
    );
  }

  Widget kotakPhoto(String url) {
    return Container(
      margin: EdgeInsets.fromLTRB(15, 0, 30, 0),
      width: 100,
      height: 100,
      decoration: BoxDecoration(
        // color: Colors.teal,
        borderRadius: BorderRadius.circular(100.0),
        image: DecorationImage(
          image: NetworkImage(url),
          fit: BoxFit.cover,
        ),
      ),
    );
  }

  Widget button(String teks) {
    return Padding(
        padding: EdgeInsets.all(5),
        child: OutlineButton(
          padding: EdgeInsets.symmetric(horizontal: 26),
          onPressed: () {},
          child: Text(
            teks,
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
        ));
  }
}
