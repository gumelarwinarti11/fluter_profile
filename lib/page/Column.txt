Column(
          children: [
            Container(
              padding: EdgeInsets.all(15),
              margin: EdgeInsets.only(bottom: 20),
              decoration: BoxDecoration(
                border: Border.all(
                  color: Colors.grey,
                  width: 0.1,
                ),
              ),
              child: Center(
                child: Text(
                  'See Covid-19 Business Resources',
                  style: TextStyle(color: Colors.cyan, fontSize: 12),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 10),
            ),
            Row(
              children: [
                kotakPhoto(
                    'https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png'),
                kotak('0', 'Posts', 16, 12),
                kotak('124', 'Followers', 16, 12),
                kotak('166', 'Following', 16, 12)
              ],
            ),
            Container(
              alignment: Alignment(-0.98, 0.0),
              padding: EdgeInsets.symmetric(horizontal: 16),
              margin: EdgeInsets.symmetric(vertical: 20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  new Text(
                    'Your Name',
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
                  ),
                  Padding(
                    padding: EdgeInsets.all(4),
                  ),
                  new Text(
                    "Your Description",
                    style: TextStyle(fontSize: 13),
                    textAlign: TextAlign.justify,
                  )
                ],
              ),
            ),
            Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  OutlineButton(
                    padding: EdgeInsets.symmetric(horizontal: 26),
                    onPressed: () async {
                      var profile = await navigateToEntryForm(context, null);
                      if (profile != null) addContact(profile);
                    },
                    child: Text(
                      'Edit Profile',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                  ),
                  button('Promotions'),
                  button('Insights')
                ],
              ),
            ),
            Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  OutlineButton(
                    padding: EdgeInsets.symmetric(horizontal: 26),
                    onPressed: () async {
                      // query();
                      nama();
                    },
                    child: Text(
                      'check',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                  ),
                ],
              ),
            ),
          ],
        )











        Container(
          child: Column(
            children: [
              Card(
                color: Colors.white,
                elevation: 2.0,
                child: ListTile(
                  leading: CircleAvatar(
                    backgroundColor: Colors.red,
                    child: Icon(Icons.people),
                  ),
                  title: Text(
                    this.contactList[index].name,
                    style: textStyle,
                  ),
                  subtitle: Text(this.contactList[index].nim),
                  trailing: GestureDetector(
                    child: Icon(Icons.delete),
                    onTap: () {
                      showDialog(
                          context: context,
                          builder: (_) => AlertDialog(
                                title: Text('Accept'),
                                content: Text('Yakin ingin Menghapus?'),
                                actions: [
                                  FlatButton(
                                    onPressed: () {
                                      Navigator.pop(context);
                                    },
                                    child: Text('No'),
                                  ),
                                  FlatButton(
                                    onPressed: () {
                                      deleteContact(contactList[index]);
                                      Navigator.pop(context);
                                    },
                                    child: Text('Yes'),
                                  )
                                ],
                                elevation: 0,
                                backgroundColor: Colors.white,
                              ),
                          barrierColor: Colors.black.withOpacity(0.7),
                          barrierDismissible: false);
                    },
                  ),
                  onTap: () async {
                    var contact = await navigateToEntryForm(
                        context, this.contactList[index]);
                    if (contact != null) editContact(contact);
                  },
                ),
              ),
              Card(
                color: Colors.white,
                elevation: 2.0,
                child: ListTile(
                  leading: CircleAvatar(
                    backgroundColor: Colors.red,
                    child: Icon(Icons.people),
                  ),
                  title: Text(
                    this.contactList[index].email,
                    style: textStyle,
                  ),
                  subtitle: Text(this.contactList[index].hobi),
                  trailing: GestureDetector(
                    child: Icon(Icons.delete),
                    onTap: () {
                      showDialog(
                          context: context,
                          builder: (_) => AlertDialog(
                                title: Text('Accept'),
                                content: Text('Yakin ingin Menghapus?'),
                                actions: [
                                  FlatButton(
                                    onPressed: () {
                                      Navigator.pop(context);
                                    },
                                    child: Text('No'),
                                  ),
                                  FlatButton(
                                    onPressed: () {
                                      deleteContact(contactList[index]);
                                      Navigator.pop(context);
                                    },
                                    child: Text('Yes'),
                                  )
                                ],
                                elevation: 0,
                                backgroundColor: Colors.white,
                              ),
                          barrierColor: Colors.black.withOpacity(0.7),
                          barrierDismissible: false);
                    },
                  ),
                  onTap: () async {
                    var contact = await navigateToEntryForm(
                        context, this.contactList[index]);
                    if (contact != null) editContact(contact);
                  },
                ),
              )
            ],
          ),
        );